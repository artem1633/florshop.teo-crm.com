<?php

use yii\db\Migration;

/**
 * Handles the creation of table `param`.
 */
class m190907_152614_create_param_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('param', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'parent_param_id' => $this->integer()->comment('Родительский параметр'),
            'type' => $this->integer()->comment('Тип'),
            'created_at' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-param-parent_param_id',
            'param',
            'parent_param_id'
        );

        $this->addForeignKey(
            'fk-param-parent_param_id',
            'param',
            'parent_param_id',
            'param',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-param-parent_param_id',
            'param'
        );

        $this->dropIndex(
            'idx-param-parent_param_id',
            'param'
        );

        $this->dropTable('param');
    }
}
