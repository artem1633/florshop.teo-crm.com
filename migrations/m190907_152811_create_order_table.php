<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m190907_152811_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'date' => $this->date()->comment('Дата'),
            'date_end' => $this->date()->comment('Дата завершение'),
            'created_at' => $this->dateTime(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order');
    }
}
