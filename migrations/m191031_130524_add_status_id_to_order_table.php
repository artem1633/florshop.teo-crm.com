<?php

use yii\db\Migration;

/**
 * Class m191031_130524_add_status_id_to_order_table
 */
class m191031_130524_add_status_id_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'status_id', $this->integer()->comment('Статус'));

        $this->createIndex(
            'idx-order-status_id',
            'order',
            'status_id'
        );

        $this->addForeignKey(
            'fk-order-status_id',
            'order',
            'status_id',
            'order_status',
            'id',
            'SET NULL'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-status_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-status_id',
            'order'
        );

        $this->dropColumn('order', 'status_id');
    }
}
