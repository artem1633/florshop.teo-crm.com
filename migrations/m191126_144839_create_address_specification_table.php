<?php

use yii\db\Migration;

/**
 * Handles the creation of table `address_specification`.
 */
class m191126_144839_create_address_specification_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('address_specification', [
            'id' => $this->primaryKey(),
            'specification' => $this->string()->comment('Спецификация'),
            'specification_path' => $this->string()->comment('Спецификация Файл'),
            'bill' => $this->string()->comment('Счет'),
            'kp' => $this->string()->comment('КП'),
            'start' => $this->string()->comment('Запуск'),
            'payment_date' => $this->date()->comment('Дата оплаты'),
            'start_date' => $this->date()->comment('Дата запуска'),
            'reload_date' => $this->date()->comment('Дата отгрузки'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropTable('address_specification');
    }
}
