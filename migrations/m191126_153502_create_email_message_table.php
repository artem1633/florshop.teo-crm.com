<?php

use yii\db\Migration;

/**
 * Handles the creation of table `email_message`.
 */
class m191126_153502_create_email_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('email_message', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('email_message');
    }
}
