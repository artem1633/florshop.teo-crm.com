<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%param_status}}`.
 */
class m200325_082525_create_param_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%param_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);

        $this->insert('param_status', [
            'name' => 'Категория',
        ]);

        $this->insert('param_status', [
            'name' => 'Направление',
        ]);

        $this->insert('param_status', [
            'name' => 'Метал',
        ]);

        $this->insert('param_status', [
            'name' => 'Замок',
        ]);

        $this->insert('param_status', [
            'name' => 'Ручка',
        ]);

        $this->insert('param_status', [
            'name' => 'Стеклопакет',
        ]);

         $this->insert('param_status', [
            'name' => 'Отбойник',
        ]);

          $this->insert('param_status', [
            'name' => 'Доводчик',
        ]);

           $this->insert('param_status', [
            'name' => 'Фрамуга',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%param_status}}');
    }
}
