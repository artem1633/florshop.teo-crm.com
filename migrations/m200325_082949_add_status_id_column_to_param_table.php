<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%param}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%param_status}}`
 */
class m200325_082949_add_status_id_column_to_param_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%param}}', 'balance', $this->integer()->comment('Остаток'));
        $this->addColumn('{{%param}}', 'multiplicity', $this->integer()->comment('Кратность'));

        // creates index for column `type`
        $this->createIndex(
            '{{%idx-param-type}}',
            '{{%param}}',
            'type'
        );

        // add foreign key for table `{{%param_status}}`
        $this->addForeignKey(
            '{{%fk-param-type}}',
            '{{%param}}',
            'type',
            '{{%param_status}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%param_status}}`
        $this->dropForeignKey(
            '{{%fk-param-type}}',
            '{{%param}}'
        );

        // drops index for column `type`
        $this->dropIndex(
            '{{%idx-param-type}}',
            '{{%param}}'
        );

        $this->dropColumn('param', 'balance');
        $this->dropColumn('param', 'multiplicity');
    }
}
