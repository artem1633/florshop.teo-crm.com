<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders_item}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%order}}`
 * - `{{%users}}`
 */
class m200325_085550_create_orders_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders_item}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'param_id' => $this->integer()->comment('Номеклатура'),
            'count' => $this->integer()->comment('Кол-во'),
            'client' => $this->string(255)->comment('Клиент'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'date_cr' => $this->datetime()->comment('Дата создание'),
            'comment' => $this->text()->comment('Комментария'),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-orders_item-param_id}}',
            '{{%orders_item}}',
            'param_id'
        );

        // add foreign key for table `{{%order}}`
        $this->addForeignKey(
            '{{%fk-orders_item-param_id}}',
            '{{%orders_item}}',
            'param_id',
            '{{%param}}',
            'id',
            'CASCADE'
        );

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-orders_item-order_id}}',
            '{{%orders_item}}',
            'order_id'
        );

        // add foreign key for table `{{%order}}`
        $this->addForeignKey(
            '{{%fk-orders_item-order_id}}',
            '{{%orders_item}}',
            'order_id',
            '{{%order}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-orders_item-user_id}}',
            '{{%orders_item}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-orders_item-user_id}}',
            '{{%orders_item}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%param}}`
        $this->dropForeignKey(
            '{{%fk-orders_item-param_id}}',
            '{{%orders_item}}'
        );

        // drops index for column `param_id`
        $this->dropIndex(
            '{{%idx-orders_item-param_id}}',
            '{{%orders_item}}'
        );

        // drops foreign key for table `{{%order}}`
        $this->dropForeignKey(
            '{{%fk-orders_item-order_id}}',
            '{{%orders_item}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-orders_item-order_id}}',
            '{{%orders_item}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-orders_item-user_id}}',
            '{{%orders_item}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-orders_item-user_id}}',
            '{{%orders_item}}'
        );

        $this->dropTable('{{%orders_item}}');
    }
}
