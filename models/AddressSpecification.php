<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address_specification".
 *
 * @property int $id
 * @property int $address_id Адрес
 * @property string $specification Спецификация
 * @property string $specification_path Спецификация Файл
 * @property string $bill Счет
 * @property string $kp КП
 * @property string $start Запуск
 * @property string $payment_date Дата оплаты
 * @property string $start_date Дата запуска
 * @property string $reload_date Дата отгрузки
 *
 * @property CompanyAddress $address
 */
class AddressSpecification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_specification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_date', 'start_date', 'reload_date'], 'required'],
            [['address_id'], 'integer'],
            [['payment_date', 'start_date', 'reload_date'], 'safe'],
            [['specification', 'specification_path', 'bill', 'kp', 'start'], 'string', 'max' => 255],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyAddress::className(), 'targetAttribute' => ['address_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address_id' => 'Адрес',
            'specification' => 'Спецификация',
            'specification_path' => 'Спецификация Файл',
            'bill' => 'Входящий счет',
            'kp' => 'КП',
            'start' => 'Исходящий счет',
            'payment_date' => 'Дата оплаты',
            'start_date' => 'Дата запуска',
            'reload_date' => 'Дата отгрузки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(CompanyAddress::className(), ['id' => 'address_id']);
    }
}
