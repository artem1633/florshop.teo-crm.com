<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "orders_item".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property int $param_id Номеклатура
 * @property int $count Кол-во
 * @property string $client Клиент
 * @property int $user_id Пользователь
 * @property string $date_cr Дата создание
 * @property string $comment Комментария
 *
 * @property Order $order
 * @property Param $param
 * @property User $user
 */
class OrdersItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'param_id', 'count', 'user_id'], 'integer'],
            [['date_cr'], 'safe'],
            [['param_id',], 'required'],
            [['comment'], 'string'],
            [['client'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['param_id'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['param_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'param_id' => 'Номеклатура',
            'count' => 'Кол-во',
            'client' => 'Клиент',
            'user_id' => 'Пользователь',
            'date_cr' => 'Дата создание',
            'comment' => 'Комментария',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
            $this->date_cr = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'param_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getParamList()
    {
        $params = Param::find()->all();
        return ArrayHelper::map($params, 'id', 'name');
    }
}
