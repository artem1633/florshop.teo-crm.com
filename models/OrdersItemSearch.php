<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrdersItem;

/**
 * OrdersItemSearch represents the model behind the search form about `app\models\OrdersItem`.
 */
class OrdersItemSearch extends OrdersItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'count', 'user_id'], 'integer'],
            [['param_id', 'client', 'date_cr', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdersItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'count' => $this->count,
            'user_id' => $this->user_id,
            'date_cr' => $this->date_cr,
        ]);

        $query->andFilterWhere(['like', 'param_id', $this->param_id])
            ->andFilterWhere(['like', 'client', $this->client])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
