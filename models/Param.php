<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "param".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $type Тип
 * @property boolean $size_counting Расчет по квадратуре
 * @property double $price Цена
 * @property string $img_url Ссылка на картинку
 * @property string $created_at
 *
 */
class Param extends \yii\db\ActiveRecord
{
    /*const TYPE_CATEGORY = 0;
//    const TYPE_SUBCATEGORY = 1;
    const TYPE_DIRECTION = 2;
    const TYPE_METAL = 3;
    const TYPE_CASTLE = 4;
    const TYPE_HANDLE = 5;
    const TYPE_GLASS_WINDOW = 6;
    const TYPE_CHIPPER = 7;
    const TYPE_CLOSER = 8;
    const TYPE_TRANSOM = 9;*/

    /**
     * @var UploadedFile
     */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'param';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'balance', 'multiplicity'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'exist', 'skipOnError' => true, 'targetClass' => ParamStatus::className(), 'targetAttribute' => ['type' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'type' => 'Группа',
            'created_at' => 'Дата и время создания',
            'balance' => 'Остаток',
            'multiplicity' => 'Кратность',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {

        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    /*public static function getTypes()
    {
        return [
            self::TYPE_CATEGORY => 'Категория',
//            self::TYPE_SUBCATEGORY => 'Подкатегория',
            self::TYPE_DIRECTION => 'Направление',
            self::TYPE_METAL => 'Метал',
            self::TYPE_CASTLE => 'Замок',
            self::TYPE_HANDLE => 'Ручка',
            self::TYPE_GLASS_WINDOW => 'Стеклопакет',
            self::TYPE_CHIPPER => 'Отбойник',
            self::TYPE_CLOSER => 'Доводчик',
            self::TYPE_TRANSOM => 'Фрамуга'
        ];
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ParamStatus::className(), ['id' => 'type']);
    }

    public function getStatusList()
    {
        $status = ParamStatus::find()->all();
        return ArrayHelper::map($status, 'id', 'name');
    }
}
