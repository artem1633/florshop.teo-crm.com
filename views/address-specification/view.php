<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AddressSpecification */
?>
<div class="address-specification-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'address_id',
            'specification',
            'specification_path',
            'bill',
            'kp',
            'start',
            'payment_date',
            'start_date',
            'reload_date',
        ],
    ]) ?>

</div>
