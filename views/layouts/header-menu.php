<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Заказы', 'icon' => 'fa  fa-rub', 'url' => ['/order'],],
                    ['label' => 'Номенклатуры', 'icon' => 'fa  fa-cog', 'url' => ['/param'],],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'],],
                    ['label' => 'Справочники', 'icon' => 'fa  fa-book', 'url' => '#', 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Примечания', 'url' => ['/comment'],],
                        ['label' => 'Статусы заказов', 'url' => ['/order-status'],],
                        ['label' => 'Группа', 'url' => ['/param-status'],],
                    ]],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
