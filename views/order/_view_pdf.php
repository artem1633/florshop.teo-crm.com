<?php

/** @var $model \app\models\Order */

use yii\widgets\DetailView;
use yii\helpers\Url;

$this->title = "Заказ №{$model->id} «{$model->name}»";

?>
<div class="header">
    <table style="width: 100%;">
        <tr>
            <td style="width: 50%;">
            </td>
            <td style="font-family: Calibri; width: 50%; font-size: 13px; line-height: 30px;">
                <p><b>ООО «КОМФОРТ»</b></p>
                <p><b>129128,Москва, ул. Малахитовая,вл.27,стр.1,Пом.35а</b></p>
                <p><b>Телефон/Факс +7(499) 390 65 55 +7 (926) 576 37 77</b></p>
                <p><b>ИНН: 7716934815/ КПП:771601001ОКПО: 36979809</b></p>
                <p><b>E-mail my-comfort@mail.ru</b></p>
            </td>
        </tr>
    </table>
</div>

<h4 style="font-family: Calibri; margin-top: 50px; text-align: center;"><?="Спецификация к заказу No{$model->id} от ".Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y')?></h4>
<p style="margin-bottom: 50px;">Адрес: <span style="font-weight: bold;"><?=$model->address?></span></p>

<div class="content">
    <?php $totalSum = 0; $counter = 0; foreach ($model->products as $product): ?>
    <?php
    $totalSum += $product->getSum();
    $productCommentPks = \yii\helpers\ArrayHelper::getColumn(\app\models\ProductComment::find()->where(['product_id' => $product->id])->all(), 'comment_id');
    $comments = \app\models\Comment::find()->where(['id' => $productCommentPks])->all();

    ?>

    <table style="width: 100%;">
        <tr>
            <td>
                <img src="<?=$product->imgPath?>" style="height: 700px;" alt="">
            </td>
            <td>
                <h3 style="font-family: Calibri; width: 1000px; font-weight: bold;"><?=$product->name?></h3>
                <?= DetailView::widget([
                    'model' => $product,
                    'attributes' => [
                        [
                            'attribute' => 'order_id',
                            'visible' => $product->order_id != null,
                        ],
                        [
                            'label' => 'Менеджер',
                            'attribute' => 'user.name',
                            'visible' => $product->user_id != null,
                        ],
                        [
                            'label' => 'Категория',
                            'attribute' => 'categoryParam.name',
                            'visible' => $product->category != null,
                        ],
                        [
                            'label' => 'Направление',
                            'attribute' => 'directionParam.name',
                            'visible' => $product->direction != null,
                        ],
                        [
                            'attribute' => 'height',
                            'visible' => $product->height != null,
                        ],
                        [
                            'attribute' => 'width',
                            'visible' => $product->width != null,
                        ],
                        [
                            'attribute' => 'additional_width',
                            'visible' => $product->additional_width != null,
                        ],
                        [
                            'attribute' => 'canvas_width',
                            'visible' => $product->canvas_width != null,
                        ],
                        [
                            'attribute' => 'ral',
                            'visible' => $product->ral != null,
                        ],
                        [
                            'label' => 'Метал',
                            'attribute' => 'metalParam.name',
                            'visible' => $product->metal != null,
                        ],
                        [
                            'label' => 'Замок',
                            'attribute' => 'castleParam.name',
                            'visible' => $product->castle != null,
                        ],
                        [
                            'label' => 'Ручка',
                            'attribute' => 'handleParam.name',
                            'visible' => $product->handle != null,
                        ],
                        [
                            'label' => 'Стеклопакет',
                            'attribute' => 'glassWindowParam.name',
                            'visible' => $product->glass_window != null,
                        ],
                        [
                            'label' => 'Отбойник',
                            'attribute' => 'chipperParam.name',
                            'visible' => $product->chipper != null,
                        ],
                        [
                            'label' => 'Доводчик',
                            'attribute' => 'closerParam.name',
                            'visible' => $product->closer != null,
                        ],
                        [
                            'label' => 'Фрамуга',
                            'attribute' => 'transomParam.name',
                            'visible' => $product->transom != null,
                        ],
                        [
                            'attribute' => 'delivery',
                            'visible' => $product->delivery != null,
                        ],
                        [
                            'attribute' => 'install',
                            'visible' => $product->install != null,
                        ],
                        [
                            'label' => 'Дата и время создания',
                            'attribute' => 'created_at',
                            'format' => ['date', 'php:d M Y H:i:s'],
                            'visible' => $product->created_at != null,
                        ],
                    ],
                ]) ?>
            </td>
        </tr>
    </table>
    <hr>
    <h4 style="font-family: Calibri;">Примечания</h4>
    <p style="font-family: Calibri;"><?=implode(', ', \yii\helpers\ArrayHelper::getColumn($comments, 'name'))?></p>
    <h4 style="font-family: Calibri; margin-top: 20px;">Дополнительная информация</h4>
    <p style="font-family: Calibri;"><?=$product->additional_information?></p>
    <h4 style="margin-top: 20px;">Сумма <?=$product->getSum()?>руб. (Кол-во: <?=$product->count?>, Цена: <?=$product->price?>руб.)</h4>
    <?php if($counter < count($model->products)-1): ?>
    <pagebreak>
        <?php endif; ?>
        <?php $counter++; ?>
        <?php endforeach; ?>
        <h4 style="margin-top: 40px; font-weight: bold;">ИТОГО ОБЩАЯ СТОИМОСТЬ ПО ЗАКАЗУ СОСТАВЛЯЕТ: <?=$totalSum?>руб. В том числе НДС.</h4>
</div>