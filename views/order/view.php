<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

$this->title = "Заказ «{$model->name}»";

?>

    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Предзаказы</h4>
                </div>
                <div class="panel-body">
                    <div id="ajaxCrudDatatable">
                        <?=GridView::widget([
                            'id'=>'crud-datatable',
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'pjax'=>true,
                            'columns' => require(__DIR__.'/_product_columns.php'),
                            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['/orders-item/create', 'order_id' => $model->id],
                                    ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-success']).'&nbsp;'.
                                Html::a('Назад', ['/order'], ['data-pjax'=>0, 'class'=>'btn btn-white', 'title'=>'Назад']),
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'showPageSummary' => true,
                            'panel' => [
                                'headingOptions' => ['style' => 'display: none;'],
                                'after'=>BulkButtonWidget::widget([
                                        'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                            ["/orders-item/bulk-delete"] ,
                                            [
                                                "class"=>"btn btn-danger btn-xs",
                                                'role'=>'modal-remote-bulk',
                                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                                'data-request-method'=>'post',
                                                'data-confirm-title'=>'Вы уверены?',
                                                'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                            ]),
                                    ]).
                                    '<div class="clearfix"></div>',
                            ]
                        ])?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация о заказе</h4>
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'name',
                            [
                                'attribute' => 'date',
                                'format' => ['date', 'php:d M Y H:i:s'],
                            ],
                            [
                                'attribute' => 'created_at',
                                'format' => ['date', 'php:d M Y H:i:s'],
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade', 'tabindex' => false],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>