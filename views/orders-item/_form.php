<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'param_id')->dropDownList($model->getParamList()) ?>

    <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'client')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
