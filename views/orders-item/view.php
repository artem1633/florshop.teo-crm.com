<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersItem */
?>
<div class="orders-item-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'param_id',
            'count',
            'client',
            'user_id',
            'date_cr',
            'comment:ntext',
        ],
    ]) ?>

</div>
