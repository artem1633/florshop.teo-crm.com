<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ParamStatus */

?>
<div class="param-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
