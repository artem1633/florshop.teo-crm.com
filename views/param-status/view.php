<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ParamStatus */
?>
<div class="param-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
