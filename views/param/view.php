<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Param */
?>
<div class="param-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'type',
            'created_at',
        ],
    ]) ?>

</div>
